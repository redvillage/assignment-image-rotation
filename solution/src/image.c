#include "image.h"
#include <stdlib.h>

void free_img(struct image* img){
    free(img);
}

/* struct image* create_img_null(){
    struct image* img = (struct image*) NULL;
    return img;
} */

struct image create_img_malloc(uint64_t width, uint64_t height){
        struct image img = {.width = width, .height = height, .data = malloc(sizeof(struct pixel) * (size_t) (width * height))};
        return img;
}
