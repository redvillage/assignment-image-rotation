#include "transform.h"
#include <stdint.h>

struct image rotate90( struct image const source ){
    struct image rotated_img = create_img_malloc(source.height, source.width);
    for(uint64_t rows = 0; rows < source.height; rows++){
        for(uint64_t cols = 0; cols < source.width; cols++){
            rotated_img.data[(source.height - 1 - rows)+ cols*source.height] = source.data[cols + rows*source.width];
        }
    }
    return rotated_img;
}
