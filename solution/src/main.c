#include "bmp.h"
#include "image.h"
#include "transform.h"


#include <stdio.h>
#include <stdlib.h>


int main( int argc, char** argv ) {
    (void) argc; (void) argv;
    FILE* input_file;
    if ((input_file = fopen(argv[1],"rb"))==NULL) {
        printf("Ошибка при открытии файла %s.\n", argv[1]);
        exit(1);
    }

    FILE* output_file;
    if ((output_file = fopen(argv[2],"wb"))==NULL) {
        printf("Ошибка при открытии файла %s.\n", argv[2]);
        exit(1);
    }

    struct image src_img = {0};
    enum read_status read_status = from_bmp(input_file, &src_img);
    printf("%d", read_status);
    struct image transformed_img = rotate90(src_img);
    enum write_status write_bmp_state = to_bmp(output_file,  &transformed_img);
    printf("%d", write_bmp_state);
    free_img(&src_img);
    free_img(&transformed_img);
    fclose(input_file);
    fclose(output_file);
    return 0;
}
