#include "image.h"
#include "bmp.h"


static struct image from_head_to_image(struct bmp_header* header, struct image* img, FILE* in){
    free_img(img);
    *img = create_img_malloc(header->biWidth, header->biHeight);
    uint8_t padding = 4 - (img->width*sizeof(struct pixel) % 4);
    for(size_t curr_row = 0; curr_row < img->height; curr_row++) {
        // read 1 row
        fread(&img->data[curr_row * (img->width)], sizeof(struct pixel), (sizeof(uint64_t) * img->width), in);
        // next row with padding taken into account
        fseek(in, padding, SEEK_CUR);
    }
    return *img;
}

static struct bmp_header bmp_header_reader(FILE* in){
    struct bmp_header header;
    fseek(in, 0, SEEK_SET);
    fread(&header, sizeof(struct bmp_header), 1, in);
    return header;
}

static struct bmp_header bmp_header_builder(uint32_t width, uint32_t height){
    uint8_t padding = 4 - (width*sizeof(struct pixel) % 4) ;
    uint32_t file_size = sizeof(struct bmp_header) + (height + width) * sizeof(struct pixel) + height * padding;
    uint32_t size_image = (height + width) * sizeof(struct pixel) + height * padding;
    struct bmp_header header = {
            .bfType         = 0x0d42, // for Windows
            .bfileSize      = file_size,
            .bfReserved     = 0,
            .bOffBits       = (uint32_t) (sizeof(uint32_t) * 12 + sizeof(uint32_t) * 3),
            .biSize         = 40,
            .biWidth        = width,
            .biHeight       = height,
            .biPlanes       = 1,
            .biBitCount     = 24,
            .biCompression  = 0,
            .biSizeImage    = size_image,
            .biXPixelsPerMeter    = 0,
            .biYPixelsPerMeter    = 0,
            .biClrUsed      = 0,
            .biClrImportant = 0
    };
    return header;
}

enum read_status from_bmp( FILE* in, struct image* img ) {
    struct bmp_header header = bmp_header_reader(in);
    *img = from_head_to_image(&header, img, in);
    return READ_OK;
    /* other enums */
}



enum write_status to_bmp( FILE* out, struct image const* img ) {
    struct bmp_header header = bmp_header_builder((uint32_t) img->width, (uint32_t)img->height);
    fseek(out, 0, SEEK_SET);
    fwrite(&header, sizeof(struct bmp_header), 1, out);
    uint8_t padding = 4 - (img->width*sizeof(struct pixel) % 4);
    for(size_t curr_row = 0; curr_row < img->height; curr_row++) {
        // read 1 row
        fread(&img->data[curr_row * (img->width)], sizeof(struct pixel), (sizeof(uint64_t) * img->width), out);
        // next row with padding taken into account
        fseek(out, padding, SEEK_CUR);
    }
    return WRITE_OK;
}
