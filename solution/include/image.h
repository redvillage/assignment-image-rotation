#ifndef IMAGE_H
#define IMAGE_H

#include <stdint.h>

struct pixel { uint8_t b, g, r; };

struct image {
    uint64_t width, height;
    struct pixel* data;
};

 // struct image* create_img_null(void);
struct image create_img_malloc(uint64_t, uint64_t);
void free_img(struct image* img);

#endif
