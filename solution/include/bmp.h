#ifndef BMP_H
#define BMP_H

#include <stdint.h>
#include <stdio.h>

#pragma pack(push, 1)
struct bmp_header
{
    uint16_t bfType; // signature, must be 4D42 hex ("BM")
    uint32_t bfileSize; // size of BMP file in bytes (unreliable)
    uint32_t bfReserved; // reserved, must be zero
    uint32_t bOffBits; // offset to start of image data in bytes
    uint32_t biSize; // size of BITMAPINFOHEADER structure, must be 40
    uint32_t biWidth; // image width in pixels
    uint32_t biHeight; // image height in pixels
    uint16_t biPlanes; // number of planes in the image, must be 1
    uint16_t biBitCount; // number of bits per pixel (1, 4, 8, or 24)
    uint32_t biCompression; // compression type (0=none, 1=RLE-8, 2=RLE-4)
    uint32_t biSizeImage; // size of image data in bytes (including padding)
    uint32_t biXPixelsPerMeter; // horizontal resolution in pixels per meter (unreliable)
    uint32_t biYPixelsPerMeter; // vertical resolution in pixels per meter (unreliable)
    uint32_t biClrUsed; // number of colors in image, or zero
    uint32_t biClrImportant; // number of important colors, or zero
};
#pragma pack(pop)

/*  deserializer   */
enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER
    /* коды других ошибок  */
};

enum read_status from_bmp( FILE* in, struct image* img );

/*  serializer   */
enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR
    /* коды других ошибок  */
};

enum write_status to_bmp( FILE* out, struct image const* img );

#endif
